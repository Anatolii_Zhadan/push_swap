/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing_helper.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/21 17:51:46 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:55:40 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_spin_stack(t_list **stack_a)
{
	t_list	*tmp;

	tmp = ft_lstlast(*stack_a);
	if (ft_callculate_mid(*stack_a) < *(int *)tmp->content)
	{
		while (!ft_checker_sort((*stack_a)))
			ft_ra(stack_a);
	}
	else
	{
		while (!ft_checker_sort((*stack_a)))
			ft_rra(stack_a);
	}
}
