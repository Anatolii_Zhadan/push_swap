/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 17:50:49 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:55:21 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

t_list	*ft_push(int num)
{
	t_list	*new;

	new = (t_list *)malloc(sizeof(t_list));
	new->content = malloc(sizeof(int));
	*(int *)new->content = num;
	new->next = NULL;
	return (new);
}

void	ft_rotate(t_list **stack)
{
	t_list	*top;
	t_list	*end;

	if (stack == NULL || (*stack)->next == NULL)
		return ;
	top = *stack;
	end = ft_lstlast(top);
	*stack = top->next;
	top->next = NULL;
	end->next = top;
}

void	ft_lstdelete_front(t_list **lst)
{
	t_list	*temp;

	if (!lst || !*lst)
		return ;
	temp = *lst;
	*lst = (*lst)->next;
	free(temp->content);
	free(temp);
}

void	ft_start_stack(t_list **stack_a, char **argv, int argc)
{
	int		i;
	char	**tmp;

	i = 0;
	if (argc == 2)
		tmp = ft_split(argv[1], ' ');
	else
	{
		tmp = argv;
		i = 1;
	}
	while (tmp[i])
	{
		ft_lstadd_front(stack_a, ft_push(ft_atoi(tmp[i])));
		i++;
	}
	if (argc == 2)
		ft_errors(tmp, 0, argc);
}

int	ft_callculate_mid(t_list *stack_a)
{
	int	sum;
	int	i;

	sum = 0;
	i = 0;
	while (stack_a)
	{
		i++;
		sum += *(int *)stack_a->content;
		stack_a = stack_a->next;
	}
	return (sum / i);
}
