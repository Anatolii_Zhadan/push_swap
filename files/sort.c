/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/07 17:24:03 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 16:57:01 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_sort_three(t_list **stack_a, int biggest)
{
	t_list	*tmp;

	tmp = *stack_a;
	biggest = *(int *)tmp->content;
	while (tmp)
	{
		if (*(int *)tmp->content > biggest)
			biggest = *(int *)tmp->content;
		tmp = tmp->next;
	}
	if (*(int *)(*stack_a)->content == biggest)
		ft_ra(stack_a);
	if (*(int *)(*stack_a)->next->content == biggest)
		ft_rra(stack_a);
	if (*(int *)(*stack_a)->content > *(int *)(*stack_a)->next->content)
		ft_sa(stack_a);
}

void	ft_find_big_min(t_list **stack_a, int *biggest, int *min)
{
	t_list	*tmp;

	tmp = *stack_a;
	*biggest = *(int *)tmp->content;
	while (tmp)
	{
		if (*(int *)tmp->content > *biggest)
			*biggest = *(int *)tmp->content;
		tmp = tmp->next;
	}
	tmp = *stack_a;
	*min = *(int *)tmp->content;
	while (tmp)
	{
		if (*(int *)tmp->content < *min)
			*min = *(int *)tmp->content;
		tmp = tmp->next;
	}
}

void	ft_sort_small(t_list **stack_a, t_list **stack_b)
{
	int	biggest;
	int	min;

	ft_find_big_min(stack_a, &biggest, &min);
	if (ft_checker_sort((*stack_a)))
		return ;
	if (ft_lstsize(*stack_a) == 2)
	{
		if (!ft_checker_sort(*stack_a))
			ft_sa(stack_a);
	}
	else if (ft_lstsize(*stack_a) == 3)
		ft_sort_three(stack_a, biggest);
	else if (ft_lstsize(*stack_a) <= 5)
		ft_sort_five(stack_a, stack_b, biggest, min);
}

int	ft_lowest_cost(t_list *st_a, t_list *st_b)
{
	int		tmp_cost;
	int		min_cost;
	int		smallest;
	int		result;
	t_list	*tmp;

	tmp = st_b;
	min_cost = 2147483647;
	while (tmp)
	{
		smallest = ft_get_best_smallest(st_a, *(int *)tmp->content);
		tmp_cost = ft_get_cost(st_a, st_b, smallest, *(int *)tmp->content);
		if (tmp_cost < min_cost)
		{
			result = *(int *)tmp->content;
			min_cost = tmp_cost;
		}
		tmp = tmp->next;
	}
	return (result);
}

int	ft_get_best_smallest(t_list *st_a, int compare)
{
	int	smallest;

	smallest = 2147483647;
	while (st_a)
	{
		if (*(int *)st_a->content > compare && smallest > *(int *)st_a->content)
			smallest = *(int *)st_a->content;
		st_a = st_a->next;
	}
	return (smallest);
}
