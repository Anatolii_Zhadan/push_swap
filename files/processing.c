/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   processing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/05 21:30:19 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:54:19 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

t_list	*ft_replace_index(int *array, t_list *stack, int size)
{
	int		num;
	int		keep;
	int		i;
	t_list	*new_stack;

	new_stack = NULL;
	num = size;
	while (stack)
	{
		keep = *(int *)stack->content;
		ft_lstdelete_front(&stack);
		i = num - 1;
		while (i >= 0)
		{
			if (array[i] == keep)
			{
				ft_lstadd_front(&new_stack, ft_push(num - i));
				break ;
			}
			i--;
		}
	}
	return (new_stack);
}

void	ft_indexing(t_list **stack, int i, int size)
{
	int		*array;
	t_list	*tmp;

	tmp = *stack;
	array = (int *)malloc(sizeof(int) * (size));
	if (array == NULL)
		return ;
	while (tmp)
	{
		array[i] = *(int *)tmp->content;
		i++;
		tmp = tmp->next;
	}
	ft_selection_sort(array, size);
	ft_reverse_array(array, size); 
	*stack = ft_replace_index(array, *stack, i);
	free(array);
}

void	ft_clean_stacks(t_list **stack_a, t_list **stack_b)
{
	t_list	*tmp;
	t_list	*main;

	main = *stack_a;
	while (main != NULL)
	{
		tmp = main->next;
		free(main->content);
		free(main);
		main = tmp;
	}
	main = *stack_b;
	while (main != NULL)
	{
		tmp = main->next;
		free(main->content);
		free(main);
		main = tmp;
	}
	*stack_a = NULL;
	*stack_b = NULL;
}

void	ft_sort_big(t_list **stack_a, t_list **stack_b)
{
	int		low_cost;
	int		next_best;
	t_list	*tmp;

	tmp = *stack_b;
	while (ft_lstsize(*stack_a) > 5 && !ft_checker_sort(*stack_a))
	{
		if (*(int *)(*stack_a)->content <= ft_callculate_mid(*stack_a))
			ft_pb(stack_a, stack_b);
		else
			ft_ra(stack_a);
	}
	ft_sort_small(stack_a, &tmp);
	while ((*stack_b))
	{
		low_cost = ft_lowest_cost(*stack_a, *stack_b);
		next_best = ft_get_best_smallest((*stack_a), low_cost);
		ft_put_on_top(stack_b, ft_lstsize((*stack_b)), low_cost, 'b');
		ft_put_on_top(stack_a, ft_lstsize((*stack_a)), next_best, 'a');
		ft_pa(stack_a, stack_b);
	}
	ft_spin_stack(stack_a);
}

void	ft_sort_five(t_list **st_a, t_list **st_b, int big, int min)
{
	while (ft_lstsize(*st_b) < 2)
	{
		if (*(int *)(*st_a)->content == min || *(int *)(*st_a)->content == big)
			ft_pb(st_a, st_b);
		else
			ft_ra(st_a);
	}
	ft_sort_three(st_a, big);
	ft_pa(st_a, st_b);
	ft_pa(st_a, st_b);
	if (*(int *)(*st_a)->content == big)
		ft_ra(st_a);
	else
	{
		ft_sa(st_a);
		ft_ra(st_a);
	}
}
