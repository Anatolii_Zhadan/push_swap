/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 20:24:52 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/14 00:42:32 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_rrb(t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL)
		return ;
	ft_rotate_rev(stack_b);
	ft_printf("rrb\n");
}

void	ft_rrr(t_list **stack_a, t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL || stack_a == NULL
		|| (*stack_a)->next == NULL)
		return ;
	ft_rotate_rev(stack_a);
	ft_rotate_rev(stack_b);
	ft_printf("rrr\n");
}

void	ft_swap(t_list **stack)
{
	int	tmp;

	if (!(*stack) || !((*stack)->next))
		return ;
	tmp = *(int *)(*stack)->content;
	*(int *)(*stack)->content = *(int *)(*stack)->next->content;
	*(int *)(*stack)->next->content = tmp;
}

int	ft_checker_sort(t_list *stack)
{
	while (stack && stack->next)
	{
		if (*(int *)stack->content > *(int *)stack->next->content)
			return (0);
		stack = stack->next;
	}
	return (1);
}

void	ft_selection_sort(int arr[], int n)
{
	int	i;
	int	j;
	int	min_idx;
	int	temp;

	i = 0;
	while (i < n - 1)
	{
		min_idx = i;
		j = i + 1;
		while (j < n)
		{
			if (arr[j] < arr[min_idx])
			{
				min_idx = j;
			}
			j++;
		}
		temp = arr[min_idx];
		arr[min_idx] = arr[i];
		arr[i] = temp;
		i++;
	}
}
