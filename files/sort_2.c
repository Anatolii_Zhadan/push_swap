/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/10 00:00:24 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:55:59 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

int	ft_moves(t_list *head, int size, int num)
{
	int	i;

	i = 0;
	while (head != NULL && *(int *)head->content != num)
	{
		i++;
		head = head->next;
	}
	if (i > (float)(size / 2))
		return (size - i);
	return (i);
}

int	ft_get_cost(t_list *st_a, t_list *st_b, int smallest, int comp)
{
	static int	i;

	i = ft_moves(st_a, ft_lstsize(st_a), smallest);
	i += ft_moves(st_b, ft_lstsize(st_b), comp);
	return (i);
}

int	ft_find_target_index(t_list **stack, int target, char flag)
{
	int		i;
	t_list	*tmp;

	tmp = *stack;
	i = 0;
	if (flag == 'a')
	{
		while (tmp != NULL && *(int *)tmp->content != target)
		{
			i++;
			tmp = tmp->next;
		}
	}
	else
	{
		while (*(int *)tmp->content != target)
		{
			i++;
			tmp = tmp->next;
		}
	}
	return (i);
}

void	ft_put_on_top(t_list **stack, int len, int target, char flag)
{
	int		i;

	i = ft_find_target_index(stack, target, flag);
	if (i > (float)(len / 2))
	{
		i = len - i;
		while (i-- != 0)
		{
			if (flag == 'a')
				ft_rra(stack);
			else
				ft_rrb(stack);
		}
	}
	else
	{
		while (i-- != 0)
		{
			if (flag == 'a')
				ft_ra(stack);
			else
				ft_rb(stack);
		}
	}
}

void	ft_reverse_array(int arr[], int n)
{
	int	temp;
	int	i;

	i = 0;
	while (i < n / 2)
	{
		temp = arr[i];
		arr[i] = arr[n - i - 1];
		arr[n - i - 1] = temp;
		i++;
	}
}
