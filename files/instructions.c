/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 19:21:30 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/13 00:52:16 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_sa(t_list **stack_a)
{
	ft_swap(stack_a);
	ft_printf("sa\n");
}

void	ft_sb(t_list **stack_b)
{
	ft_swap(stack_b);
	ft_printf("sb\n");
}

void	ft_ss(t_list **stack_a, t_list **stack_b)
{
	ft_swap(&*stack_a);
	ft_swap(&*stack_b);
	ft_printf("ss\n");
}

void	ft_pa(t_list **stack_a, t_list **stack_b)
{
	int	tmp;

	if (*stack_b == NULL)
		return ;
	tmp = *(int *)(*stack_b)->content;
	ft_lstadd_front(stack_a, ft_push(tmp));
	ft_lstdelete_front(stack_b);
	ft_printf("pa\n");
}

void	ft_pb(t_list **stack_a, t_list **stack_b)
{
	int	tmp;

	if (*stack_a == NULL)
		return ;
	tmp = *(int *)(*stack_a)->content;
	ft_lstadd_front(stack_b, ft_push(tmp));
	ft_lstdelete_front(stack_a);
	ft_printf("pb\n");
}
