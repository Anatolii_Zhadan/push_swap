NAME := push_swap
NAME_BONUS := checker

FILES := files/push_swap.c files/stack.c files/instructions.c\
			files/instructions_2.c files/instructions_3.c \
			files/processing.c files/sort.c files/sort_2.c \
			files/processing_helper.c
FILES_BONUS := bonus/checker.c bonus/instructions.c bonus/instructions_2.c \
			bonus/instructions_3.c bonus/helper.c bonus/helper_2.c bonus/helper_3.c
OFILES := $(FILES:.c=.o)
OFILES_BONUS := $(FILES_BONUS:.c=.o)


LIBFT := libft/libft.a

CFLAGS := -Wall -Wextra -Werror
CC := cc 

all: $(NAME)

$(NAME):$(OFILES)
	@make -C libft/ 
	@$(CC) $(CFLAGS) $(OFILES) $(LIBFT) -o $(NAME)

$(NAME_BONUS):$(OFILES_BONUS)
	@make -C libft/ 
	@$(CC) $(CFLAGS) $(OFILES_BONUS) $(LIBFT) -o $(NAME_BONUS)

bonus: $(NAME_BONUS)

clean:
	@rm -f $(OFILES) $(OFILES_BONUS)

fclean: clean
	@rm -f $(NAME) $(NAME_BONUS)

re: fclean all

.phony: all, clean, fclean re