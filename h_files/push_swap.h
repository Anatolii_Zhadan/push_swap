/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/24 20:38:37 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:53:19 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"

// push_swap.c
int		ft_check_num(char *str);
void	ft_errors(char **tmp, int flag, int argc);
int		ft_double(char **argv, int i, int co);
void	ft_checker(t_list **stack_a, char **argv, int argc, int i);
// stack.c
int		ft_callculate_mid(t_list *stack_a);
void	ft_start_stack(t_list **stack_a, char **argv, int argc);
t_list	*ft_push(int num);
void	ft_lstdelete_front(t_list **lst);
void	ft_rotate(t_list **stack);
// instructions.c
void	ft_sa(t_list **stack_a);
void	ft_sb(t_list **stack_b);
void	ft_ss(t_list **stack_a, t_list **stack_b);
void	ft_pa(t_list **stack_a, t_list **stack_b);
void	ft_pb(t_list **stack_a, t_list **stack_b);
// instructions_2.c
void	ft_rotate_rev(t_list **stack);
void	ft_ra(t_list **stack_a);
void	ft_rb(t_list **stack_b);
void	ft_rr(t_list **stack_a, t_list **stack_b);
void	ft_rra(t_list **stack_a);
// instructions_3.c
void	ft_rrb(t_list **stack_b);
void	ft_rrr(t_list **stack_a, t_list **stack_b);
void	ft_swap(t_list **stack);
int		ft_checker_sort(t_list *stack);
void	ft_selection_sort(int arr[], int n);
// processing.c
t_list	*ft_replace_index(int *array, t_list *stack, int i);
void	ft_indexing(t_list **stack, int i, int size);
void	ft_clean_stacks(t_list **stack_a, t_list **stack_b);
void	ft_sort_big(t_list **stack_a, t_list **stack_b);
void	ft_sort_five(t_list **st_a, t_list **st_b, int big, int min);
//processing_helper.c
void	ft_spin_stack(t_list **stack_a);
//sort.c
void	ft_sort_three(t_list **stack_a, int biggest);
void	ft_sort_small(t_list **stack_a, t_list **stack_b);
void	ft_find_big_min(t_list **stack_a, int *biggest, int *min);
int		ft_lowest_cost(t_list *stack_a, t_list *stack_b);
int		ft_get_best_smallest(t_list *st_a, int compare);
//sort_2.c
int		ft_get_cost(t_list	*st_a, t_list *st_b, int smallest, int comp);
int		ft_find_target_index(t_list **stack, int target, char flag);
void	ft_put_on_top(t_list **stack, int len, int target, char flag);
void	ft_reverse_array(int arr[], int n);
int		ft_moves(t_list *head, int size, int num);

#endif