/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/18 02:49:31 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/19 23:58:29 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "../libft/libft.h"

//checker.c
void	ft_checker(t_list **stack_a, char **argv, int argc, int i);
int		ft_double(char **argv, int i, int co);
void	ft_errors(char **tmp, int flag, int argc);
int		ft_check_num(char *str);
//instructions
void	ft_pb(t_list **stack_a, t_list **stack_b);
void	ft_pa(t_list **stack_a, t_list **stack_b);
void	ft_ss(t_list **stack_a, t_list **stack_b);
void	ft_sb(t_list **stack_b);
void	ft_sa(t_list **stack_a);
//instructions_2
void	ft_rra(t_list **stack_a);
void	ft_rr(t_list **stack_a, t_list **stack_b);
void	ft_rb(t_list **stack_b);
void	ft_ra(t_list **stack_a);
void	ft_rotate_rev(t_list **stack);
//instructions_3
void	ft_lstdelete_front(t_list **lst);
void	ft_checker_sort_special(t_list **stack_a, t_list **stack_b);
void	ft_swap(t_list **stack);
void	ft_rrr(t_list **stack_a, t_list **stack_b);
void	ft_rrb(t_list **stack_b);
//helper.c
void	ft_clean_stacks(t_list **stack_a, t_list **stack_b);
void	ft_start_stack(t_list **stack_a, char **argv, int argc);
t_list	*ft_push(int num);
void	ft_rotate(t_list **stack);
void	ft_read_instructions(t_list **stack_a, t_list **stack_b);
//helper_2.c
int		ft_correct_instruction(char *tmp, char **instructions);
void	ft_implement(t_list **stack_a, t_list **stack_b, char *instr);
int		ft_strcmp(char *s1, char *s2);
void	ft_indexing(t_list **stack, int i, int size);
void	ft_selection_sort(int arr[], int n);
//helper_3.c
void	ft_reverse_array(int arr[], int n);
t_list	*ft_replace_index(int *array, t_list *stack, int size);

#endif