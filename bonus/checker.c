/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/18 02:49:04 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/19 23:59:04 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/checker.h"

int	ft_check_num(char *str)
{
	int	i;

	i = 0;
	if (str[i] == '-')
		i++;
	if (!str[i])
		return (0);
	while (str[i])
	{
		if (!(str[i] <= '9' && str[i] >= '0'))
			return (0);
		i++;
	}
	return (1);
}

void	ft_errors(char **tmp, int flag, int argc)
{
	int	i;

	if (argc == 2)
	{
		i = -1;
		while (tmp[++i])
			free(tmp[i]);
		free(tmp);
	}
	if (flag == 1)
	{
		write(2, "Error\n", 6);
		exit(1);
	}
	if (flag == 2)
		exit(0);
}

int	ft_double(char **argv, int i, int co)
{
	if (argv[1] == NULL)
		ft_errors(argv, 2, 2);
	while (argv[++i])
		if (ft_atoi(argv[i]) == co)
			return (1);
	return (0);
}

void	ft_checker(t_list **stack_a, char **argv, int argc, int i)
{
	char		**tmp;
	long long	num;

	if (argc == 2)
		tmp = ft_split(argv[1], ' ');
	else
	{
		tmp = argv;
		i = 1;
	}
	while (tmp[i])
	{
		num = ft_atoi(tmp[i]);
		if (!ft_check_num(tmp[i]))
			ft_errors(tmp, 1, argc);
		if (num < -2147483648 || num > 2147483647)
			ft_errors(tmp, 1, argc);
		if (ft_double(tmp, i, ft_atoi(tmp[i])))
			ft_errors(tmp, 1, argc);
		i++;
	}
	if (argc == 2)
		ft_errors(tmp, 0, argc);
	ft_start_stack(stack_a, argv, argc);
}

int	main(int argc, char **argv)
{
	t_list	*stack_a;
	t_list	*stack_b;

	if (argc > 1)
	{
		stack_a = NULL;
		stack_b = NULL;
		ft_checker(&stack_a, argv, argc, 0);
		if (stack_a == NULL)
			return (0);
		ft_indexing(&stack_a, 0, ft_lstsize(stack_a));
		ft_read_instructions(&stack_a, &stack_b);
	}
	return (0);
}
