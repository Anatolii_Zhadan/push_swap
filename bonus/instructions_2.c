/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 18:03:02 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/18 03:28:58 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_rotate_rev(t_list **stack)
{
	t_list	*top;
	t_list	*end;

	if (stack == NULL || (*stack)->next == NULL)
		return ;
	top = *stack;
	end = ft_lstlast(top);
	while (top)
	{
		if (top->next->next == NULL)
		{
			top->next = NULL;
			break ;
		}
		top = top->next;
	}
	end->next = *stack;
	*stack = end;
}

void	ft_ra(t_list **stack_a)
{
	if (stack_a == NULL || (*stack_a)->next == NULL)
		return ;
	ft_rotate(stack_a);
}

void	ft_rb(t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL)
		return ;
	ft_rotate(stack_b);
}

void	ft_rr(t_list **stack_a, t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL \
		|| stack_a == NULL || (*stack_a)->next == NULL)
		return ;
	ft_rotate(stack_a);
	ft_rotate(stack_b);
}

void	ft_rra(t_list **stack_a)
{
	if (stack_a == NULL || (*stack_a)->next == NULL)
		return ;
	ft_rotate_rev(stack_a);
}
