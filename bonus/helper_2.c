/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/18 21:01:38 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/21 17:54:57 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/checker.h"

int	ft_correct_instruction(char *tmp, char **instructions)
{
	int	i;

	i = -1;
	while (instructions[++i])
	{
		if (!ft_strcmp(instructions[i], tmp))
			break ;
		else if (!instructions[i + 1])
			return (0);
	}
	return (1);
}

void	ft_implement(t_list **stack_a, t_list **stack_b, char *tmp)
{
	if (!ft_strcmp(tmp, "pb\n"))
		ft_pb(stack_a, stack_b);
	else if (!ft_strcmp(tmp, "pa\n"))
		ft_pa(stack_a, stack_b);
	else if (!ft_strcmp(tmp, "ss\n"))
		ft_ss(stack_a, stack_b);
	else if (!ft_strcmp(tmp, "sb\n"))
		ft_sb(stack_b);
	else if (!ft_strcmp(tmp, "sa\n"))
		ft_sa(stack_a);
	else if (!ft_strcmp(tmp, "rra\n"))
		ft_rra(stack_a);
	else if (!ft_strcmp(tmp, "rrb\n"))
		ft_rrb(stack_b);
	else if (!ft_strcmp(tmp, "rrr\n"))
		ft_rrr(stack_a, stack_b);
	else if (!ft_strcmp(tmp, "rr\n"))
		ft_rr(stack_a, stack_b);
	else if (!ft_strcmp(tmp, "rb\n"))
		ft_rb(stack_b);
	else if (!ft_strcmp(tmp, "ra\n"))
		ft_ra(stack_a);
}

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
		i++;
	return (s1[i] - s2[i]);
}

void	ft_indexing(t_list **stack, int i, int size)
{
	int		*array;
	t_list	*tmp;

	tmp = *stack;
	array = (int *)malloc(sizeof(int) * (size));
	if (array == NULL)
		return ;
	while (tmp)
	{
		array[i] = *(int *)tmp->content;
		i++;
		tmp = tmp->next;
	}
	ft_selection_sort(array, size);
	ft_reverse_array(array, size); 
	*stack = ft_replace_index(array, *stack, i);
	free(array);
}

void	ft_selection_sort(int arr[], int n)
{
	int	i;
	int	j;
	int	min_idx;
	int	temp;

	i = 0;
	while (i < n - 1)
	{
		min_idx = i;
		j = i + 1;
		while (j < n)
		{
			if (arr[j] < arr[min_idx])
			{
				min_idx = j;
			}
			j++;
		}
		temp = arr[min_idx];
		arr[min_idx] = arr[i];
		arr[i] = temp;
		i++;
	}
}
