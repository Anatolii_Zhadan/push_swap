/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/18 16:54:56 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/20 00:30:49 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/checker.h"

void	ft_clean_stacks(t_list **stack_a, t_list **stack_b)
{
	t_list	*tmp;
	t_list	*main;

	main = *stack_a;
	while (main != NULL)
	{
		tmp = main->next;
		free(main->content);
		free(main);
		main = tmp;
	}
	main = *stack_b;
	while (main != NULL)
	{
		tmp = main->next;
		free(main->content);
		free(main);
		main = tmp;
	}
	*stack_a = NULL;
	*stack_b = NULL;
}

void	ft_start_stack(t_list **stack_a, char **argv, int argc)
{
	int		i;
	char	**tmp;

	i = 0;
	if (argc == 2)
		tmp = ft_split(argv[1], ' ');
	else
	{
		tmp = argv;
		i = 1;
	}
	while (tmp[i])
	{
		ft_lstadd_front(stack_a, ft_push(ft_atoi(tmp[i])));
		i++;
	}
	if (argc == 2)
		ft_errors(tmp, 0, argc);
}

t_list	*ft_push(int num)
{
	t_list	*new;

	new = (t_list *)malloc(sizeof(t_list));
	new->content = malloc(sizeof(int));
	*(int *)new->content = num;
	new->next = NULL;
	return (new);
}

void	ft_rotate(t_list **stack)
{
	t_list	*top;
	t_list	*end;

	if (stack == NULL || (*stack)->next == NULL)
		return ;
	top = *stack;
	end = ft_lstlast(top);
	*stack = top->next;
	top->next = NULL;
	end->next = top;
}

void	ft_read_instructions(t_list **stack_a, t_list **stack_b)
{
	char	*all_instructions;
	char	**instructions;
	char	*tmp;

	all_instructions = "pb\n pa\n ss\n sb\n sa\n rra\n rr\n rb\n ra\n rrr\n rrb\n";
	instructions = ft_split(all_instructions, ' ');
	while (1)
	{
		tmp = get_next_line(0);
		if (!tmp)
			break ;
		if (ft_correct_instruction(tmp, instructions))
			ft_implement(stack_a, stack_b, tmp);
		else
		{
			free(tmp);
			ft_clean_stacks(stack_a, stack_b);
			ft_errors(instructions, 1, 2);
		}
		free(tmp);
	}
	ft_checker_sort_special(stack_a, stack_b);
	ft_clean_stacks(stack_a, stack_b);
	ft_errors(instructions, 2, 2);
}
