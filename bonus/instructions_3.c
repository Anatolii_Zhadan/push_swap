/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/01 20:24:52 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/19 23:57:41 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/push_swap.h"

void	ft_rrb(t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL)
		return ;
	ft_rotate_rev(stack_b);
}

void	ft_rrr(t_list **stack_a, t_list **stack_b)
{
	if (stack_b == NULL || (*stack_b)->next == NULL || stack_a == NULL
		|| (*stack_a)->next == NULL)
		return ;
	ft_rotate_rev(stack_a);
	ft_rotate_rev(stack_b);
}

void	ft_swap(t_list **stack)
{
	int	tmp;

	if (!(*stack) || !((*stack)->next))
		return ;
	tmp = *(int *)(*stack)->content;
	*(int *)(*stack)->content = *(int *)(*stack)->next->content;
	*(int *)(*stack)->next->content = tmp;
}

void	ft_checker_sort_special(t_list **stack_a, t_list **stack_b)
{
	t_list	*tmp;

	tmp = *stack_a;
	if ((*stack_b))
	{
		ft_printf("KO\n");
		return ;
	}
	while (tmp && tmp->next)
	{
		if (*(int *)tmp->content > *(int *)tmp->next->content)
		{
			ft_printf("KO\n");
			return ;
		}
		tmp = tmp->next;
	}
	ft_printf("OK\n");
}

void	ft_lstdelete_front(t_list **lst)
{
	t_list	*temp;

	if (!lst || !*lst)
		return ;
	temp = *lst;
	*lst = (*lst)->next;
	free(temp->content);
	free(temp);
}
