/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/19 23:34:21 by azhadan           #+#    #+#             */
/*   Updated: 2023/07/19 23:37:36 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/checker.h"

void	ft_reverse_array(int arr[], int n)
{
	int	temp;
	int	i;

	i = 0;
	while (i < n / 2)
	{
		temp = arr[i];
		arr[i] = arr[n - i - 1];
		arr[n - i - 1] = temp;
		i++;
	}
}

t_list	*ft_replace_index(int *array, t_list *stack, int size)
{
	int		num;
	int		keep;
	int		i;
	t_list	*new_stack;

	new_stack = NULL;
	num = size;
	while (stack)
	{
		keep = *(int *)stack->content;
		ft_lstdelete_front(&stack);
		i = num - 1;
		while (i >= 0)
		{
			if (array[i] == keep)
			{
				ft_lstadd_front(&new_stack, ft_push(num - i));
				break ;
			}
			i--;
		}
	}
	return (new_stack);
}
